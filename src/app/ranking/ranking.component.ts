import { Component, OnInit } from '@angular/core';
import { RankingService } from '../shared/services/ranking.service';

@Component({
  selector: 'star-quiz-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {
  ranking: { name: string, email: string, score: number };
  displayedColumns: string[] = ['name', 'email', 'score'];

  constructor(private rankingService: RankingService) { }

  ngOnInit() {
    this.ranking = this.rankingService.getRanking();
  }

}
