import { Component } from '@angular/core';

@Component({
  selector: 'star-quiz-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'star-quiz';
}
