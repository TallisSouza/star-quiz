import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameRoutingModule } from './game-routing.module';
import { GameComponent } from './game.component';
import { StartGameComponent } from './start-game/start-game.component';
import { StartCountdownComponent } from './start-countdown/start-countdown.component';
import { MaterialModule } from '../shared/material/material.module';
import { CountdownComponent } from './countdown/countdown.component';
import { AnswersStatusComponent } from './answers-status/answers-status.component';
import { PersonListComponent } from './person-list/person-list.component';
import { PersonComponent } from './person/person.component';
import { PersonDetailsModalComponent } from './person-details-modal/person-details-modal.component';
import { GuessModalComponent } from './guess-modal/guess-modal.component';
import { FormsModule } from '@angular/forms';
import { ResultModalComponent } from './result-modal/result-modal.component';

@NgModule({
  declarations: [
    GameComponent,
    StartGameComponent,
    StartCountdownComponent,
    CountdownComponent,
    AnswersStatusComponent,
    PersonListComponent,
    PersonComponent,
    PersonDetailsModalComponent,
    GuessModalComponent,
    ResultModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    GameRoutingModule,
    MaterialModule
  ],
  entryComponents: [PersonDetailsModalComponent, GuessModalComponent, ResultModalComponent]
})
export class GameModule { }
