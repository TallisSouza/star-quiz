import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { GameStatus } from 'src/app/shared/models/game-status';
import { GameService } from 'src/app/shared/services/game.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'star-quiz-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.scss']
})
export class StartGameComponent implements OnInit {
  @Output() started: EventEmitter<GameStatus> = new EventEmitter<GameStatus>();

  gameStatus: GameStatus = new GameStatus();
  hasGameInProgress = false;
  startCountdownSubject: Subject<void> = new Subject<void>();
  startedCountdown = false;
  startedGame = false;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.hasGameInProgress = this.gameService.hasGameInProgress();
  }

  startCountdown(loadGame: boolean): void {
    this.startedCountdown = true;
    this.gameStatus = loadGame ? this.gameStatus = this.gameService.getGameInProgress() : this.gameStatus;
    this.startCountdownSubject.next();
  }

  finishedCountdown(): void {
    this.startedGame = true;
    this.started.emit(this.gameStatus);
  }

}
