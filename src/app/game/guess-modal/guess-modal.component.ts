import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'star-quiz-guess-modal',
  templateUrl: './guess-modal.component.html',
  styleUrls: ['./guess-modal.component.scss']
})
export class GuessModalComponent {
  userGuess: string;

  constructor(
    private dialogRef: MatDialogRef<GuessModalComponent>
  ) { }

  makeGuess(form: NgForm) {
    this.dialogRef.close({ guess: form.form.value.guess });
  }

  cancel() {
    this.dialogRef.close();
  }

}
