import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessModalComponent } from './guess-modal.component';

describe('GuessModalComponent', () => {
  let component: GuessModalComponent;
  let fixture: ComponentFixture<GuessModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
