import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'star-quiz-start-countdown',
  templateUrl: './start-countdown.component.html',
  styleUrls: ['./start-countdown.component.scss']
})
export class StartCountdownComponent implements OnInit, OnDestroy {
  @Input() startCountdown: Subject<void>;
  @Output() finishedCountdown: EventEmitter<void> = new EventEmitter<void>();

  startCountdownSubscription: Subscription;
  currentTime: number;

  constructor() { }

  ngOnInit() {
    this.startCountdownSubscription = this.startCountdown.subscribe(() => this.start());
  }

  ngOnDestroy() {
    this.startCountdownSubscription.unsubscribe();
  }

  start() {
    this.currentTime = 3;
    const interval = setInterval(
      () => {
        this.currentTime--;
        if (this.currentTime === 0) {
          clearInterval(interval);
          this.finishedCountdown.emit();
        }
      }, 1000);
  }

}
