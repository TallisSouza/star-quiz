import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswersStatusComponent } from './answers-status.component';

describe('AnswersStatusComponent', () => {
  let component: AnswersStatusComponent;
  let fixture: ComponentFixture<AnswersStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswersStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswersStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
