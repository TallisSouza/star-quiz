import { Component, Input } from '@angular/core';
import { GameStatus } from 'src/app/shared/models/game-status';

@Component({
  selector: 'star-quiz-answers-status',
  templateUrl: './answers-status.component.html',
  styleUrls: ['./answers-status.component.scss']
})
export class AnswersStatusComponent {
  @Input() gameStatus: GameStatus;
}
