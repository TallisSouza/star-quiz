import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Person } from 'src/app/shared/models/person';
import { Guess } from 'src/app/shared/models/guess';
import { MatDialog } from '@angular/material/dialog';
import { PersonDetailsModalComponent } from '../person-details-modal/person-details-modal.component';
import { PeopleService } from 'src/app/shared/services/people.service';
import { GuessModalComponent } from '../guess-modal/guess-modal.component';
import { GameStatus } from 'src/app/shared/models/game-status';

const CLASS_CORRECT = 'correct';
const CLASS_INCORRECT = 'incorrect';
@Component({
  selector: 'star-quiz-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  @Input() person: Person;
  @Input()
  get gameStatus(): GameStatus {
    return this._gameStatus;
  }

  set gameStatus(gameStatus: GameStatus) {
    this._gameStatus = gameStatus;
    const guessIndex = this._gameStatus.guesses.findIndex(({person}) => person.id === this.person.id);
    if (guessIndex !== -1) {
      this.currentGuess = new Guess(this._gameStatus.guesses[guessIndex]);
      this.setClassName();
    }
  }
  @Output() guessed: EventEmitter<Guess> = new EventEmitter<Guess>();
  @Output() countdownStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

  currentGuess: Guess = new Guess();
  className = '';
  _isLoading = false;
  private _gameStatus: GameStatus;

  get isLoading(): boolean {
    return this.isLoading;
  }

  set isLoading(value: boolean) {
    this._isLoading = value;
    this.countdownStatus.emit(!value);
  }

  constructor(private matDialog: MatDialog) { }

  ngOnInit() {
    this.currentGuess.person = this.person;
  }

  guess() {
    this.matDialog.open(GuessModalComponent, {
      width: '300px'
    }).afterClosed().subscribe(
      (response) => {
        if (response && response.guess) {
          this.currentGuess.guessedName = response.guess;
          this.setClassName();
          this.guessed.emit(this.currentGuess);
        }
      }
    );
  }

  showModalInfo() {
    this.currentGuess.seenDetails = true;
    this.matDialog.open(PersonDetailsModalComponent, {
      width: '400px',
      minWidth: '300px',
      data: this.person
    });
  }

  private setClassName() {
    this.className = this.currentGuess.isAnswerCorrect() ? CLASS_CORRECT : CLASS_INCORRECT;
  }

}
