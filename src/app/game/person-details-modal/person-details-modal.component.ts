import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Person } from 'src/app/shared/models/person';

@Component({
  selector: 'star-quiz-person-details-modal',
  templateUrl: './person-details-modal.component.html',
  styleUrls: ['./person-details-modal.component.scss']
})
export class PersonDetailsModalComponent {

  constructor(
    private dialogRef: MatDialogRef<PersonDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public person: Person
  ) { }

  close() {
    this.dialogRef.close();
  }

}
