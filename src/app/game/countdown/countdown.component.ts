import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'star-quiz-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy {
  @Input('timer') _timer: number;
  @Input() pause: Subject<void>;
  @Input() start: Subject<number>;
  @Output() finished: EventEmitter<void> = new EventEmitter<void>();
  @Output() timerChange: EventEmitter<number> = new EventEmitter<number>();

  intervalListener: any;
  dateTimeCountdown: Date;
  isPaused = false;
  destroyComponent$ = new Subject<void>();

  get timer(): number {
    return this._timer;
  }

  set timer(value: number) {
    this.timerChange.emit(value);
    this._timer = value;
  }

  ngOnInit() {
    this.start.pipe(
      takeUntil(this.destroyComponent$)
    ).subscribe(
      (value: number) => {
        this.timer = value;
        this.isPaused = false;
        this.startCountdown();
      });
    this.pause.pipe(
      takeUntil(this.destroyComponent$)
    ).subscribe(
      () => {
        this.isPaused = true;
        clearInterval(this.intervalListener);
      }
    );
  }

  ngOnDestroy() {
    this.destroyComponent$.next();
    this.destroyComponent$.complete();
  }

  startCountdown(): void {
    this.dateTimeCountdown = new Date(this.timer * 1000);
    clearInterval(this.intervalListener);
    this.intervalListener = setInterval(() => {
      this.timer--;
      this.dateTimeCountdown = new Date(this.timer * 1000);
      if (this.dateTimeCountdown.getTime() === 0) {
        this.finishTime();
      }
    }, 1000);
  }

  finishTime(): void {
    clearInterval(this.intervalListener);
    this.finished.emit();
  }
}
