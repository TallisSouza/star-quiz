import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameStatus } from 'src/app/shared/models/game-status';
import { NgForm } from '@angular/forms';
import { RankingService } from 'src/app/shared/services/ranking.service';
import { Router } from '@angular/router';
import { GameService } from 'src/app/shared/services/game.service';

@Component({
  selector: 'star-quiz-result-modal',
  templateUrl: './result-modal.component.html',
  styleUrls: ['./result-modal.component.scss']
})
export class ResultModalComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ResultModalComponent>,
    @Inject(MAT_DIALOG_DATA) public gameStatus: GameStatus,
    private rakningService: RankingService,
    private gameService: GameService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

  saveResult(form: NgForm) {
    if (form.form.valid) {
      this.rakningService.saveResult({gameStatus: this.gameStatus, userData: form.form.value});
      this.gameService.clearGame();
      this.router.navigateByUrl('/ranking');
      this.dialogRef.close();
    }
  }
}
