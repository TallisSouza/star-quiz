import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { GameStatus } from '../shared/models/game-status';
import { GameService } from '../shared/services/game.service';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ResultModalComponent } from './result-modal/result-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'star-quiz-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit, OnDestroy {
  gameStatus: GameStatus;
  pauseSubject: Subject<void> = new Subject<void>();
  startSubject: Subject<number> = new Subject<number>();

  constructor(private gameService: GameService, private matDialog: MatDialog, private router: Router) { }

  ngOnInit() {
    this.gameStatus = new GameStatus();
  }

  ngOnDestroy() {
    this.saveProgress();
  }

  @HostListener('window:beforeunload')
  public saveProgress(): void {
    if (this.gameStatus.timer !== 120 && this.gameStatus.timer !== 0) {
      this.gameService.saveGameInProgress(this.gameStatus);
    } else {
      this.gameService.clearGame();
    }
  }

  startGame(gameStatus: GameStatus): void {
    this.gameStatus = gameStatus;
    this.startSubject.next(this.gameStatus.timer);
  }

  timesUp() {
    this.matDialog.open(ResultModalComponent, {
      width: '40%',
      minWidth: '300px',
      data: this.gameStatus
    }).afterClosed().subscribe(
      () => {
        this.gameService.clearGame();
        this.router.navigateByUrl('/');
      }
    );
  }

  countdownStatusChanged(running: boolean) {
    if (running) {
      this.startSubject.next(this.gameStatus.timer);
    } else {
      this.pauseSubject.next();
    }
  }
}
