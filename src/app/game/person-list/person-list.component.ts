import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { GameStatus } from 'src/app/shared/models/game-status';
import { PeopleService } from 'src/app/shared/services/people.service';
import { Guess } from 'src/app/shared/models/guess';
import { GameService } from 'src/app/shared/services/game.service';
import { Person } from 'src/app/shared/models/person';
import { catchError, map, startWith, takeUntil, tap } from 'rxjs/operators';
import { combineLatest, EMPTY, Observable, Subject } from 'rxjs';

const ITEMS_PER_PAGE = 10;

@Component({
  selector: 'star-quiz-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit, OnDestroy {
  @Input() gameStatus: GameStatus;
  @Output() countdownStatus: EventEmitter<boolean> = new EventEmitter<boolean>()
  @Output() gameStatusChange: EventEmitter<GameStatus> = new EventEmitter<GameStatus>();

  currentPage = 1;
  shownPeople$ = new Observable<Person[]>();
  hasNextPage$ = new Observable<boolean>();
  hasPreviousPage$ = new Observable<boolean>();
  hasError = false;
  _isLoading = false;
  changedPageSubject$ = new Subject<number>();
  destroyComponent$ = new Subject<void>();

  get isLoading(): boolean {
    return this._isLoading;
  }

  set isLoading(value: boolean) {
    this._isLoading = value;
    this.countdownStatus.emit(!value);
  }

  constructor(
    private peopleService: PeopleService,
    private gameService: GameService
  ) { }

  ngOnInit(): void {
    const pagedPeople$ = combineLatest([
      this.peopleService.getPeopleV2(),
      this.changedPageSubject$.asObservable().pipe(
        startWith(this.currentPage)
      )
    ]);
    this.shownPeople$ = pagedPeople$.pipe(
      takeUntil(this.destroyComponent$),
      tap(() => this.isLoading = false),
      catchError(() => {
        this.hasError = true;
        return EMPTY;
      }),
      map(([people, pageNumber]) =>
        people.slice((pageNumber - 1) * ITEMS_PER_PAGE, pageNumber * ITEMS_PER_PAGE)
      )
    );
    this.hasNextPage$ = pagedPeople$.pipe(
      takeUntil(this.destroyComponent$),
      map(([people, pageNumber]) => pageNumber * ITEMS_PER_PAGE <= people.length)
    );
    this.hasPreviousPage$ = pagedPeople$.pipe(
      takeUntil(this.destroyComponent$),
      map(([, pageNumber]) => pageNumber > 1)
    );
  }

  ngOnDestroy(): void {
    this.destroyComponent$.next();
    this.destroyComponent$.complete();
  }

  changePage(action: string): void {
    if (action === 'next') {
      this.currentPage = this.currentPage + 1;
    } else if (action === 'previous') {
      this.currentPage = this.currentPage - 1;
    }
    this.changedPageSubject$.next(this.currentPage);
  }

  guessed(guess: Guess): void {
    this.gameStatus.guesses = [...this.gameStatus.guesses, guess];
    this.gameStatusChange.emit(this.gameStatus);
    this.gameService.saveGameInProgress(this.gameStatus);
  }

  countdownChanged(countdownRunning: boolean): void {
    this.countdownStatus.emit(countdownRunning);
  }
}
