import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'star-quiz-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  backgroundImage: string;

  constructor() { }

  ngOnInit() {
    this.backgroundImage = 'welcome-image-1.jpg';
  }

}
