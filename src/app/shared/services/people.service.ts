import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Person } from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private httpClient: HttpClient) { }

  getPeopleV2(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(`${environment.apiBaseUrl}/all.json`);
  }

}
