import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GameStatus } from '../models/game-status';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor() { }

  hasGameInProgress(): boolean {
    return !!localStorage.getItem(environment.localStorage.game);
  }

  saveGameInProgress(game: GameStatus): void {
    localStorage.setItem(environment.localStorage.game, JSON.stringify(game));
  }

  getGameInProgress(): GameStatus {
    return new GameStatus(JSON.parse(localStorage.getItem(environment.localStorage.game)));
  }

  clearGame(): void {
    localStorage.removeItem(environment.localStorage.game);
  }
}
