import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GameStatus } from '../models/game-status';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  constructor() { }

  getRanking() {
    const rankingData = JSON.parse(localStorage.getItem(environment.localStorage.ranking))
    return rankingData === null ? [] : rankingData;
  }

  saveResult(result: { gameStatus: GameStatus, userData: { name: string, email: string } }) {
    let ranking: any[] = this.getRanking();
    ranking.push({
      name: result.userData.name,
      email: result.userData.email,
      score: result.gameStatus.totalScore
    });
    ranking = this.sortRanking(ranking);
    localStorage.setItem(environment.localStorage.ranking, JSON.stringify(ranking));
  }

  private sortRanking(ranking: { name: string, email: string, score: number }[]) {
    return ranking.sort(
      (userA, userB) => {
        if (userA.score > userB.score) {
          return 1;
        } else if (userA.score === userB.score) {
          if (userA.name > userB.name) {
            return 1;
          } else if (userA.name === userB.name) {
            return 0;
          } else {
            return -1;
          }
        } else {
          return -1;
        }
      });
  }
}
