import { Person } from './person';

export class Guess {
  guessedName: string;
  person: Person;
  seenDetails: boolean;

  constructor(guess?: Guess) {
    this.seenDetails = guess ? guess.seenDetails : false;
    this.person = guess ? guess.person : new Person();
    this.guessedName = guess ? guess.guessedName : '';
  }

  isAnswerCorrect(): boolean {
    return this.guessedName.toLowerCase() === this.person.name.toLowerCase();
  }
}
