export class Person {
  id: number;
  name: string;
  image: string;
  homeworld: string;
  eyeColor: string;
  skinColor: string;
  hairColor: string;
  species: string | string[];
  height: string;
  mass: string;
  gender: string;
  born: string;
  bornLocation: string;
}
