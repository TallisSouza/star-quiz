import { environment } from 'src/environments/environment';
import { Guess } from './guess';
import { Person } from './person';

export class GameStatus {
  timer: number;
  correctAnswers: number;
  incorrectAnswers: number;
  totalScore: number;
  private _guesses: Guess[];

  get guesses(): Guess[] {
    return this._guesses;
  }

  set guesses(value: Guess[]) {
    this._guesses = value;
    this.calculateScore();
  }

  constructor(gameStatus?: GameStatus) {
    this.timer = gameStatus ? gameStatus.timer : environment.gameConfigurations.time;
    this.correctAnswers = gameStatus ? gameStatus.correctAnswers : 0;
    this.incorrectAnswers = gameStatus ? gameStatus.incorrectAnswers : 0;
    this.totalScore = gameStatus ? gameStatus.totalScore : 0;
    this.guesses = gameStatus ? gameStatus._guesses : [];
  }

  calculateScore() {
    let score = 0;
    let correctAnswers = 0;
    let incorrectAnswers = 0;
    this._guesses.forEach(
      (guess: Guess) => {
        guess = new Guess(guess);
        if (guess.isAnswerCorrect()) {
          correctAnswers++;
          score += !guess.seenDetails ? 10 : 5;
        } else {
          incorrectAnswers++;
        }
      }
    );
    this.totalScore = score;
    this.correctAnswers = correctAnswers;
    this.incorrectAnswers = incorrectAnswers;
  }
}
