export const environment = {
  production: false,
  localStorage: {
    game: 'gameDataDev',
    ranking: 'rankingDataDev'
  },
  gameConfigurations: {
    time: 120
  },
  apiBaseUrl: 'https://rawcdn.githack.com/akabab/starwars-api/0.2.1/api/'
};
